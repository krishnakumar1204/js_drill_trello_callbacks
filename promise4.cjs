/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/


const getBoardInfo = require("./promise1.cjs");
const getAllLists = require("./promise2.cjs");
const getAllCards = require("./promise3.cjs");



function getCardsFromMind(boardsPath, listsPath, cardsPath, boardID) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof listsPath === 'string' && listsPath !== '') && (typeof cardsPath === 'string' && cardsPath !== '') && (typeof boardID === 'string' && boardID !== '')) {

        getBoardInfo(boardsPath, boardID)
            .then((boardWithGivenId) => {
                console.log(`Information of ${boardWithGivenId.name} Board: `, boardWithGivenId);

                return getAllLists(listsPath, boardID);
            })
            .then((listsWithGivenId) => {
                console.log(`\nLists for Thanos Board: `, listsWithGivenId);

                const mindObj = listsWithGivenId.find((list) => list.name === "Mind");
                const mindId = mindObj.id;

                return getAllCards(cardsPath, mindId);
            })
            .then((mindCard) => {
                console.log("\nCards for the Mind List: ", mindCard);
            })
            .catch((err) => {
                console.log(err);
            })
    }
    else {
        console.log("Please give valid data");
    }
}


module.exports = getCardsFromMind;