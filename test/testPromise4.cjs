const getCardsFromMind = require("../promise4.cjs");

const boardsPath = "../data/boards.json";
const listsPath = "../data/lists_1.json";
const cardsPath = "../data/cards.json";

const boardId = "mcu453ed";

getCardsFromMind(boardsPath, listsPath, cardsPath, boardId);