const getBoardInfo = require("../promise1.cjs");
const boardsPath = "../data/boards.json";

let boardId = "";

getBoardInfo(boardsPath, boardId)
    .then((boardWithGivenId) => {
        console.log(`Board for ${boardWithGivenId.name}: `);
        console.log(boardWithGivenId);
    })
    .catch((err) => {
        console.log(err);
    })
