const getAllLists = require("../callback2.cjs");
const listsPath = "../data/lists_1.json";

let boardId = "abc122dc";

getAllLists(listsPath, boardId, (error, listsWithGivenId) => {
    
    if(error){
        console.error(error);
    }
    else{
        console.log(listsWithGivenId);
    }
});