const getAllCards = require("../promise3.cjs");
const cardsPath = "../data/cards.json";

const listId = "cffv432";

getAllCards(cardsPath, listId)
    .then((cardsWithGivenId) => {
        console.log("Cards with the given List Id: ");
        console.log(cardsWithGivenId);
    })
    .catch((err) => {
        console.log(err);
    })