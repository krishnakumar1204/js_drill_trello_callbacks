const getBoardInfo = require("../callback1.cjs");
const boardsPath = "../data/boards.json";

let boardId = "abc122dc";

getBoardInfo(boardsPath, boardId, (error, boardWithGivenId) => {
    
    if (error) {
        console.error(error);
    }
    else {
        console.log(boardWithGivenId);
    }

});
