const getAllCards = require("../callback3.cjs");
const cardsPath = "../data/cards.json";

const listId = "cffv432";

getAllCards(cardsPath, listId, (error, cardsWithGivenId) => {
    
    if(error){
        console.error(error);
    }
    else{
        console.log(cardsWithGivenId);
    }
});