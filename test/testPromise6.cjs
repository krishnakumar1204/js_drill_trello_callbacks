const getCardsFromAllList = require("../promise6.cjs");

const boardsPath = "../data/boards.json";
const listsPath = "../data/lists_1.json";
const cardsPath = "../data/cards.json";

const boardId = "mcu453ed";

getCardsFromAllList(boardsPath, listsPath, cardsPath, boardId);