const getAllLists = require("../promise2.cjs");
const listsPath = "../data/lists_1.json";

let boardId = "abc122dc";

getAllLists(listsPath, boardId)
    .then((listsWithGivenId) => {
        console.log("Lists with the given Board Id: ");
        console.log(listsWithGivenId);
    })
    .catch((err) => {
        console.log(err);
    })