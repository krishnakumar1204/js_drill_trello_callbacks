/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getAllCards(cardsPath, listId, callback) {

    if ((typeof cardsPath === 'string' && cardsPath !== '') && (typeof listId === 'string' && listId !== '') && typeof callback === 'function') {

        setTimeout(() => {

            fs.readFile(cardsPath, 'utf-8', (error, cardsData) => {
                
                if (error) {
                    callback(new Error("Unable to read the file"), null);
                }
                else {
                    cardsData = JSON.parse(cardsData);
                    const cardsWithGivenId = cardsData[listId];

                    if (cardsWithGivenId) {
                        callback(null, cardsWithGivenId);
                    }
                    else {
                        callback(new Error("Cards for the given list Id not found!"), null);
                    }
                }
            })

        }, 2000);
    }
    else {
        console.log("Please give valid data");
    }
}

module.exports = getAllCards;