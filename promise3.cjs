/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getAllCards(cardsPath, listId) {

    if ((typeof cardsPath === 'string' && cardsPath !== '') && (typeof listId === 'string' && listId !== '')) {

        return new Promise((resolve, reject) => {

            fs.readFile(cardsPath, 'utf-8', (error, cardsData) => {

                if (error) {
                    reject("Unable to read the file");
                }
                else {
                    cardsData = JSON.parse(cardsData);
                    const cardsWithGivenId = cardsData[listId];

                    if (cardsWithGivenId) {
                        setTimeout(() => {
                            resolve(cardsWithGivenId);
                        })
                    }
                    else {
                        reject("Cards for the given list Id not found!");
                    }
                }
            })

        });
    }
    else {
        return Promise.reject("Please give valid data");
    }
}

module.exports = getAllCards;