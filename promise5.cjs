/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


const getBoardInfo = require("./promise1.cjs");
const getAllLists = require("./promise2.cjs");
const getAllCards = require("./promise3.cjs");



function getCardsFromMindAndSpace(boardsPath, listsPath, cardsPath, boardID) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof listsPath === 'string' && listsPath !== '') && (typeof cardsPath === 'string' && cardsPath !== '') && (typeof boardID === 'string' && boardID !== '')) {

        getBoardInfo(boardsPath, boardID)
            .then((boardWithGivenId) => {
                console.log(`Information of ${boardWithGivenId.name} Board: `, boardWithGivenId);

                return getAllLists(listsPath, boardID);
            })
            .then((listsWithGivenId) => {
                console.log(`\nLists for Thanos Board: `, listsWithGivenId);

                const mindSpaceObj = listsWithGivenId.filter((list) => (list.name === "Mind") || (list.name === "Space"));
                const mindSpaceId = mindSpaceObj.map((list) => list.id);
                const mindSpacePromises = mindSpaceId.map((id) => getAllCards(cardsPath, id));

                return Promise.all(mindSpacePromises);
            })
            .then((mindSpaceCards) => {
                console.log("\nCards for Mind List: ", mindSpaceCards[0]);
                console.log("\nCards for Space List: ", mindSpaceCards[1]);
            })
            .catch((err) => {
                console.log(err);
            })
    }
    else {
        console.log("Please give valid data");
    }
}


module.exports = getCardsFromMindAndSpace;