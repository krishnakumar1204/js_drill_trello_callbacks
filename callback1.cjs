/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getBoardInfo(boardsPath, boardId, callback) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof boardId === 'string' && boardId !== '') && typeof callback === 'function') {

        setTimeout(() => {

            fs.readFile(boardsPath, 'utf-8', (error, boardsData) => {

                if (error) {
                    callback(new Error("Unable to read the file!"), null);
                }
                else {

                    boardsData = JSON.parse(boardsData);
                    const boardWithGivenId = boardsData.find(board => board.id === boardId);

                    if (boardWithGivenId) {
                        callback(null, boardWithGivenId);
                    }
                    else {
                        callback(new Error("Board for the given board Id not found!"), null);
                    }
                }

            })

        }, 2000);
        
    }
    else{
        console.log("Please give valid data");
    }
}

module.exports = getBoardInfo;