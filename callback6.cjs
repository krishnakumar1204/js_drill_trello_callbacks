/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/


const getBoardInfo = require("./callback1.cjs");
const getAllLists = require("./callback2.cjs");
const getAllCards = require("./callback3.cjs");


function getCardsFromAllList(boardsPath, listsPath, cardsPath, boardID) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof listsPath === 'string' && listsPath !== '') && (typeof cardsPath === 'string' && cardsPath !== '')) {

        setTimeout(() => {
            getBoardInfo(boardsPath, boardID, (error, boardWithGivenId) => {

                if (error) {
                    console.log(error);
                }
                else {

                    console.log("Informations of Thanos boards: ");
                    console.log(boardWithGivenId);

                    getAllLists(listsPath, boardID, (error, listsWithGivenId) => {

                        if (error) {
                            console.log(error);
                        }
                        else {

                            console.log("\nLists for Thanos boards:");
                            // console.log(listsWithGivenId);

                            const allId = listsWithGivenId.map((list) => list.id);
                            let index = 0;

                            function getCards(index, allId) {

                                if (index < allId.length) {

                                    getAllCards(cardsPath, allId[index], (error, cardData) => {

                                        if (error) {
                                            // console.log(error);
                                            console.log(`\nCards for the ${listsWithGivenId[index].name} list not found!`);
                                            console.log(listsWithGivenId[index]);
                                        }
                                        else {

                                            console.log(`\ncards for the ${listsWithGivenId[index].name} list:`);
                                            console.log(listsWithGivenId[index]);
                                            console.log(cardData);

                                            getCards(index + 1, allId);
                                        }
                                    })
                                }
                            }

                            getCards(index, allId);
                        }
                    })
                }
            })
        }, 2000);
    }
    else {
        console.log("Please give valid data");
    }
}


module.exports = getCardsFromAllList;