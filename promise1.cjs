/* 
    Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getBoardInfo(boardsPath, boardId) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof boardId === 'string' && boardId !== '')) {

        return new Promise((resolve, reject) => {

            fs.readFile(boardsPath, 'utf-8', (error, boardsData) => {

                if (error) {
                    reject("Unable to read the file!");
                }
                else {

                    boardsData = JSON.parse(boardsData);
                    const boardWithGivenId = boardsData.find(board => board.id === boardId);

                    if (boardWithGivenId) {
                        setTimeout(() => {
                            resolve(boardWithGivenId);
                        }, 3000);
                    }
                    else {
                        reject("Board for the given board Id not found!");
                    }
                }

            })
        })

    }
    else {
        return Promise.reject("Please give valid data");
    }
}

module.exports = getBoardInfo;