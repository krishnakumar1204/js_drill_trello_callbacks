/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/


const getBoardInfo = require("./callback1.cjs");
const getAllLists = require("./callback2.cjs");
const getAllCards = require("./callback3.cjs");



function getCardsFromMind(boardsPath, listsPath, cardsPath, boardID) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof listsPath === 'string' && listsPath !== '') && (typeof cardsPath === 'string' && cardsPath !== '')) {

        setTimeout(() => {

            getBoardInfo(boardsPath, boardID, (error, boardWithGivenId) => {

                if (error) {
                    console.log(error);
                }
                else {

                    console.log("Informations of Thanos boards: ");
                    console.log(boardWithGivenId);

                    getAllLists(listsPath, boardID, (error, listsWithGivenId) => {
                        if (error) {
                            console.log(error);
                        }
                        else {

                            console.log("\nLists for Thanos boards:");
                            console.log(listsWithGivenId);
                            const mindObj = listsWithGivenId.find((list) => list.name === 'Mind');
                            const mindId = mindObj.id;
                            getAllCards(cardsPath, mindId, (error, mindCard) => {
                                if (error) {
                                    console.log(error);
                                }
                                else {
                                    console.log("\nCards for the Mind List:");
                                    console.log(mindCard);
                                }
                            })
                        }
                    })
                }
            })
        }, 2000);
    }
    else {
        console.log("Please give valid data");
    }
}


module.exports = getCardsFromMind;