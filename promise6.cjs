/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardInfo = require("./promise1.cjs");
const getAllLists = require("./promise2.cjs");
const getAllCards = require("./promise3.cjs");


function getCardsFromAllList(boardsPath, listsPath, cardsPath, boardID) {

    if ((typeof boardsPath === 'string' && boardsPath !== '') && (typeof listsPath === 'string' && listsPath !== '') && (typeof cardsPath === 'string' && cardsPath !== '') && (typeof boardID === 'string' && boardID !== '')) {

        getBoardInfo(boardsPath, boardID)
            .then((boardWithGivenId) => {
                console.log(`Information of ${boardWithGivenId.name} Board: `, boardWithGivenId);

                return getAllLists(listsPath, boardID);
            })
            .then((listsWithGivenId) => {

                let index=0;

                function getCards(listsWithGivenId, index){

                    if(index < listsWithGivenId.length){

                        getAllCards(cardsPath, listsWithGivenId[index].id)
                            .then((card) => {

                                console.log(`\nList for ${listsWithGivenId[index].name}: `, listsWithGivenId[index]);
                                console.log(`Cards for ${listsWithGivenId[index].name} List: `, card);

                                getCards(listsWithGivenId, index+1);
                            })
                            .catch((err) => {

                                // console.log(err);
                                console.log(`\nCards for the ${listsWithGivenId[index].name} List not found!`);
                            })
                    }
                }

                getCards(listsWithGivenId, index);

            })
            .catch((err) => {
                console.log(err);
            })
    }
    else {
        console.log("Please give valid data");
    }
}


module.exports = getCardsFromAllList;