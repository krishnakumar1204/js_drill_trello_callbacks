/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getAllLists(listsPath, boardId) {

    if ((typeof listsPath === 'string' && listsPath !== '') && (typeof boardId === 'string' && boardId !== '')) {

        return new Promise((resolve, reject) => {

            fs.readFile(listsPath, 'utf-8', (error, listsData) => {

                if (error) {
                    reject("Unable to read the file!");
                }
                else {

                    listsData = JSON.parse(listsData);
                    const listsWithGivenId = listsData[boardId];

                    if (listsWithGivenId) {
                        setTimeout(() => {
                            resolve(listsWithGivenId);
                        })
                    }
                    else {
                        reject("Lists for the given board id not found!");
                    }
                }

            })

        })

    }
    else {
        return Promise.reject("Please give valid data");
    }
}

module.exports = getAllLists;