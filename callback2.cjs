/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require("fs");

function getAllLists(listsPath, boardId, callback) {

    if ((typeof listsPath === 'string' && listsPath !== '') && (typeof boardId === 'string' && boardId !== '') && typeof callback === 'function') {

        setTimeout(() => {

            fs.readFile(listsPath, 'utf-8', (error, listsData) => {

                if (error) {
                    callback(new Error("Unable to read the file!"), null);
                }
                else {

                    listsData = JSON.parse(listsData);
                    const listsWithGivenId = listsData[boardId];

                    if (listsWithGivenId) {
                        callback(null, listsWithGivenId);
                    }
                    else {
                        callback(new Error("Lists for the given board id not found!"), null);
                    }
                }

            })

        }, 2000);

    }
    else {
        console.log("Please give valid data");
    }
}

module.exports = getAllLists;